#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak
#----------------------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid = os.fork() #pid del proceso hijo, copia del proceso padre, es decir, copia del programa
				# si es 0 = hijo
if pid != 0:
	os.wait() #no lo ejecutará hasta que acabe el hijo
	print("Programa pare",os.getpid(), pid)
else:
	print("Programa fill",os.getpid(), pid)

print("See you soon bro")
sys.exit(0)
