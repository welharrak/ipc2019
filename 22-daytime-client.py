#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# Crear un daytime client/server amb un popen de date. El server plega un cop respost.
# En aquest cas el client pot ser qualsevol eina client que simplement es connecta i
# escolta la resposta del servidor.
# El client es connecta al servidor  i aquest li retorna la data i tanca la conexió. El
# client mostra l adata rebuda i en veure que s'ha tancat la connexió també finalitza.
# El servidor engega i espera a rebre una conexió, quan l'accepta executa un Popen per
# fer un date del sistema operatiu, retorna la informació i tanca la conexió. També
# finalitza (és un servidor molt poc treballador!!)
#----------------------------------------------------
import sys,socket
#HOST = '3.9.165.48' #ip ews ec2
HOST = ''
PORT = 50001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((HOST, PORT))

while True:
	data = s.recv(1024)
	if not data: break
	print('Received', repr(data))
	

s.close()


sys.exit(0)
