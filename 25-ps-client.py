#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# **25-ps-client-one2one.py [-p port] server**

# Els clients es connecten a un servidor, envien un informe consistent en fer
# "ps ax" i finalitzen la connexió. El servidor rep l'informe del client i
# el desa a disc. Cada informe es desa amb el format: ip-port-timestamt.log, on
# timestamp té el format AADDMM-HHMMSS.
# Usar un servidor com el de l'exercici anterior, un daemon governat per senyals.
#
#----------------------------------------------------
import sys,socket, argparse
from subprocess import Popen, PIPE
#------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description='Server ps')
parser.add_argument('-p','--port',type=int,help='port server',dest='port',default=50001)
parser.add_argument('server',type=str,help='server host',metavar='server')

args = parser.parse_args()
#----------------------------------------------------------------------------------------
HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.connect((HOST, PORT))

command = 'ps'
pipeData = Popen(command,stdout=PIPE,shell=True)
	
for line in pipeData.stdout:
	s.send(line) 
	
s.close()
sys.exit(0)
