#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#walid el harrak

# 07-list-users.py   [-f file]
# Donat un file tipus /etc/passwd o stdin (amb aquest format) fer:

# el constructor d'objectes UnixUser rep un strg amb la línia sencera
# tipus /etc/passwd.
# llegir línia a línia cada usuari assignant-lo a un objecte
# UnixUser i afegir l’usuari a una llista d’usuaris UnixUser.
# un cop completada la carrega de dades i amb la llista amb
# tots els usuaris, llistar per pantalla els usuaris recorrent la llista.

import argparse

# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, uid:%d, gid=%d" % (self.login,self.uid, self.gid))

  def __str__(self):
    "functió to_string"
    return "%s %d %d" % (self.login, self.uid, self.gid)
# -------------------------------------------------------

parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
parser.add_argument("-f","--fit",type=str,\
        help="user file or stdin (/etc/passwd style)", metavar="file",\
        default="/dev/stdin",dest="fitxer")
args=parser.parse_args()

fileIn=open(args.fitxer,"r")
userList=[]
for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
fileIn.close()
for user in userList:
 print(user)

		
