#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#walid el harrak

#**05-head-multi.py [-n 5|10|15 ] [-f file ...]

import sys
import argparse

def head(fitxer,lines):
	'''
	'''
	count = 0
	for line in fitxer:
		
		count += 1
		
		print(line,end='')
		
		if count == lines:
			break
		

parser = argparse.ArgumentParser(description='Mostar les n primeres línies (o 10)  de filein (o stdin).',epilog='See you soon baby')

parser.add_argument('-n','--nlines',type=int,dest='nlin',help='Linies a mostrar',choices=[5,10,15],default=10)

parser.add_argument('-f','--fitxer',type=str,dest='fitxerList',help='file to process',nargs='*',required=True)

parser.add_argument('-v','--verbose',action='store_true',dest='verbose',help='mostra capçalera')


args = parser.parse_args()

lines = args.nlin


for fitxer in args.fitxerList:
	
	fileIn = open(fitxer,'r')
	if args.verbose:
		print('============================',fitxer,'============================')
	
	head(fileIn,lines)
	
	
