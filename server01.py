#!/usr/bin/python3
#-*- coding: utf-8-*-

# walid el harrak
# isx48144165
# 6/2/2020

#-------------------------------------------------------------------
import socket, sys, signal, os, argparse
from subprocess import Popen, PIPE

#----------------------------------------------------------------------
def mysigusr1(signum,frame):
	print("Signal handler called with signal:", signum)
	print('Connexions:')
	for connexio in connexions:
		print(connexio)
	sys.exit(0)

def mysigusr2(signum,frame):
	print("Signal handler called with signal:", signum)
	print('Connexions:',len(connexions))
	sys.exit(0)

def mysigterm(signum,frame):
	print("Signal handler called with signal:", signum)
	print('Connexions:',len(connexions))
	for connexio in connexions:
		print(connexio)
	sys.exit(0)
	
#--------------------------------------------------------------
parser = argparse.ArgumentParser(description='Server que mostra una ordere al client depenen de la petició d\'aquest')
parser.add_argument('-d','--debug',action="store_true")
parser.add_argument('-p','--port',type=int,help='port server',dest='port',default=44444)


args = parser.parse_args()

#------------------------------------------------------------------------------
HOST = ''
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind((HOST,PORT))
s.listen(1)

command = ''
FI = chr(4).encode('utf-8')

connexions = []

#-------------------------------------------------------------------------
pid=os.fork()
if pid !=0:
  print("Engegat el server CAL:", pid)
  sys.exit(0)
  
#--------------------------------------------------------------------------------
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
#per ignorar la senyal de SIGINT:
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.signal(signal.SIGKILL,signal.SIG_IGN)

#------------------------------------------------------------------------------
while True:
	conn, addr = s.accept()
	connexions.append(addr)
	
	print("Connected by", addr)
	while True:	
		data = conn.recv(1024)
		if not data: break

		if data == b'processos\n':
			command = 'ps ax'
			
		elif data == b'ports\n':
			command = 'netstat -puta'
			
		else:
			command = 'uname -a'
	
		pipeData = Popen(command,shell=True,stdout=PIPE,bufsize=0,universal_newlines=True)
	
		for line in pipeData.stdout:
			conn.sendall(line.encode('utf-8'))
			if args.debug: print(str(line.encode('utf-8')))
			
		#en aquest cas no fa fer sortida error, ja que les ordes no vàlides 
		#s'executaran com 'uname -a'
			
		conn.send(FI)
	conn.close()
		
sys.exit(0)
	
	
