#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#walid el harrak

#**04-head-choices.py [-n 5|10|15]   file**

#  Mostar les n primeres 5 o 10 o 15 (def 10)  de filein.
#  Usar choices de argparse per definir un conjunt de valors vàlids.

import sys
import argparse

parser = argparse.ArgumentParser(description='Mostar les n primeres línies (o 10)  de filein (o stdin).',epilog='See you soon baby')

parser.add_argument('-n','--nlines',type=int,dest='nlin',help='Linies a mostrar',choices=[5,10,15],default=10)

parser.add_argument('-f','--fitxer',type=str,dest='fitxer',help='file to process',required=True)

args = parser.parse_args()

###########################


lines = args.nlin
	

fileIn = open(args.fitxer,'r')

count = 0

for line in fileIn:
	
	count += 1
	
	print(line, end='')
	
	if count == lines:
		break
		
fileIn.close()
