#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

#**10-count-by-group.py [-s gid | gname | nusers ] fileUsuaris fileGrups**

#  LListar els grups del sistema ordenats pel criteri de gname, gid o de 
#  número d'usuaris.
#  *Atenció* cal gestionar apropiadament la duplicitat dels usuaris en un grup.
#  Requeriment: desar a la llista d'usuaris del grup tots aquells usuaris
#  que hi pertanyin, sense duplicitats, tant com a grup principal com a 
#  grup secundari.

import argparse
userList = []

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
    
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid))

  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)

class UnixGroup():
	'''
	Classe grup de unix prototipus /etc/group
	gname:passwd:gid:listusers
	'''
	def __init__(self,groupLine):
		groupField=groupLine.split(':')
		self.gid = int(groupField[2])
		self.gname = groupField[0]
		self.passwd = groupField[1]
		self.users = groupField[3][:-1].split(',')
		self.users = list(filter(None, self.users))
		for user in userList:
			if (user.gid == self.gid) and (user.login not in self.users):
				self.users.append(user.login) 
		
		
	def __str__(self):
		"functió to_string"
		return "%s %s %d %s" %(self.gname, self.passwd, self.gid, self.users)	
#------------------------------------------------------------------------------------------------
def sort_gid(group):
	return (group.gid)

def sort_gname(group):
	return (group.gname, group.gid)

def sort_nusers(group):
	return (len(group.users), group.gid)
#------------------------------------------------------------------------------------------------------

parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",epilog="thats all folks")

parser.add_argument('-s','--sort',type=str,dest='sort',help='Mostrar dependen del ordre posat',choices=['nusers','gid','gname'],default='gid')

parser.add_argument('userfile',type=str,metavar='userfile',help='userfile')

parser.add_argument('groupfile',type=str,metavar='groupfile',help='groupfile')

args = parser.parse_args()
#--------------------------------------------------------------------------------------------------------

#processem fitxer users i creem llista d'users
fileIn=open(args.userfile,"r")


for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
  
fileIn.close()


#---------------------------------------------------------------------------------------------------
fileIn=open(args.groupfile,"r")

groupList = []
for line in fileIn:
	group=UnixGroup(line)
	groupList.append(group)

fileIn.close()

#--------------------------------------------------------------------------------------------------------
if args.sort == 'gid':
	groupList.sort(key=sort_gid)
	
elif args.sort == 'gname':
	groupList.sort(key=sort_gname)
	
else:
	groupList.sort(key=sort_nusers, reverse=True)
	
for group in groupList:
	print(group)
