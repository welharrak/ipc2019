#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# 26-telnetServer.py [-p port] [-d debug]
# Implementar un servidor i un client telnet. Client i server fan un diàleg.
# Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de cada connexió.	
#------------------------------------------------------------------------------
import sys,socket, argparse, os
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description='Server ps')
parser.add_argument('-p','--port',type=int,help='port server',dest='port',default=50001)
parser.add_argument('-d','--debug',action="store_true")

args = parser.parse_args()

HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #stream = TCP
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para que el socket se pueda reutilizar aunque pete

s.bind((HOST,PORT))
s.listen(1) #activa que escolti

FI = chr(4).encode('utf-8')

while True:
	conn, addr = s.accept() #comença a escoltar "at infinitum"fins que rebi UN missatge, retorna tupla
	print("Connected by", addr)
	while True:
		command = conn.recv(1024)
		if not command: break
		
		pipeData = Popen(command,stdout=PIPE,stderr=PIPE,shell=True,bufsize=0,universal_newlines=True) #shell=True, lo ejecuta con bash
		for line in pipeData.stdout:
			conn.sendall(line.encode('utf-8'))
			if args.debug: print(str(line.encode('utf-8')))
		
		for line in pipeData.stderr:
			conn.sendall(line.encode('utf-8'))
			if args.debug: print(str(line.encode('utf-8')))
			
		conn.send(FI)
	break
	
conn.close()
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
