#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# exemple execv
#----------------------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid = os.fork()
if pid != 0:
    print("Programa pare",os.getpid(),pid)
    sys.exit(0)

print("Programa fill", os.getpid(),pid)
#con execv : os.execv(path, args)
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"]) #executa el proces
                                                #fill com un ls(en aquest cas)
                                                #por lo tanto, lo de despues no se ejecuta
#con execl: os.execl(path, arg0, arg1, ...)
#os.execl("/usr/bin/ls","/usr/bin/ls","-ll","/home/users/inf/hisx2/isx48144165")

#con execlp: os.execlp(file, arg0, arg1, ...)
#os.execlp("ls","ls","-ll","/home/users/inf/hisx2/isx48144165")

#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-ls","/")
#os.execlp("ls","ls","-ls","/")
#os.execvp("uname",["uname","-a"])
#os.execv("/usr/bin/bash",["/usr/bin/bash","/var/tmp/m06-aso/ipc2019/show.sh"])
os.execle("/usr/bin/bash","/usr/bin/bash","/var/tmp/m06-aso/ipc2019/show.sh",{"nom":"joan","edat":"25"}) #el ultimo campo es el env 
																										# que se pone como diccionario


print("BYE!")
sys.exit(0)
