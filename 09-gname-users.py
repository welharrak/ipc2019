#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  walid el harrak

# 09-gname-users.py [-s login(por defecto) | gid | gname] -u fileuser -g filegroup

import argparse
groupDic = {}

class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupDic: self.gname=groupDic[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
    
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid))

  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)

class UnixGroup():
	'''
	Classe grup de unix prototipus /etc/group
	gname:passwd:gid:listusers
	'''
	def __init__(self,groupLine):
		groupField=groupLine.split(':')
		self.gid = int(groupField[2])
		self.gname = groupField[0]
		self.passwd = groupField[1]
		self.userList = groupField[3][:-1]
		
	def __str__(self):
		"functió to_string"
		return "%s %s %d %s" %(self.gname, self.passwd, self.gid, self.userList)	
#------------------------------------------------------------------------------------------------

def sort_login(user):
	return user.login
	
def sort_gid(user):
	return (user.gid, user.login)

def sort_gname(user):
	return (user.gname, user.login)

#------------------------------------------------------------------------------------------------------

parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",epilog="thats all folks")

parser.add_argument('-s','--sort',type=str,dest='sort',help='Mostrar dependen del ordre posat',choices=['login','gid','gname'],default='login')

parser.add_argument('-u','--userfile',type=str,metavar='userfile',help='usefile',required=True)

parser.add_argument('-g','--groupfile',type=str,metavar='groupfile',help='groupfile',required=True)

args = parser.parse_args()

#--------------------------------------------------------------------------------------------------------
#processem fitxer group i creem i carreguem diccionari
fileIn=open(args.groupfile,"r")


for line in fileIn:
	group=UnixGroup(line)
	groupDic[group.gid]=group

fileIn.close()

#--------------------------------------------------------------------------------------------------------
#processem fitxer users i creem llista d'users
fileIn=open(args.userfile,"r")
userList=[]

for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
  
fileIn.close()

#--------------------------------------------------------------------------------------------------------
if args.sort == 'gid':
	userList.sort(key=sort_gid)
	
elif args.sort == 'login':
	userList.sort(key=sort_login)
	
else:
	userList.sort(key=sort_gname)
	
for user in userList:
	print(user)


