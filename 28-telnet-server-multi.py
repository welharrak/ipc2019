#!/usr/bin/python3
#-*- coding: utf-8-*-

#walid el gharrak
# como el 26 pero multi
#-------------------------------------------------------------------
import socket, sys, select, os, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description='Server ps')
parser.add_argument('-p','--port',type=int,help='port server',dest='port',default=50001)
parser.add_argument('-d','--debug',action="store_true")

args = parser.parse_args()

HOST = ''
PORT = args.port
#------------------------------------------------------------------------------------------
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para que el socket se pueda reutilizar aunque pete

s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
conns=[s] #s es el socket que escolta
FI = chr(4).encode('utf-8')
while True:
    actius,x,y = select.select(conns,[],[]) #actius conté una llista de les connexions/sockets on hi ha activitat
    for actual in actius: # per cada s que s'ha mogut (s es cada connexion nueva!!!)
        if actual == s: # si es una nova conexio
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else: #en les connexions ja obertes previament
            command = actual.recv(1024)
            if not command:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close() #tanca connexio
                conns.remove(actual) #treure clinet
            else:
                pipeData = Popen(command,stdout=PIPE,stderr=PIPE,shell=True,bufsize=0,universal_newlines=True) #shell=True, lo ejecuta con bash
                for line in pipeData.stdout:
                    actual.sendall(line.encode('utf-8'))
                    if args.debug: print(str(line.encode('utf-8')))
                    
                for line in pipeData.stderr:
                    actual.sendall(line.encode('utf-8'))
                    if args.debug: print(str(line.encode('utf-8')))
                
                actual.sendall(FI)
s.close()
sys.exit(0)
