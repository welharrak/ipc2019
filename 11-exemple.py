# /usr/bin/python3
#-*- coding: utf-8-*
# exemple-popen.py
#----------------------------------------------------
#  walid el harrak
#-----------------------------------------------------
import argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="Exemple Popen")

parser.add_argument("ruta",type=str,help="directori a llistar")

args=parser.parse_args()
#------------------------------------------------------------

command = ["ls",args.ruta]

pipeData = Popen(command,stdout=PIPE)

#Popen = construcor de pipe, execute en un extrem command i l'altre sdout=PIPE

#stdout=PIPE, dice que el stout de la commanda tiene que ser PIPE

#python escucha DE LO QUE SALE POR LA TUBERIA(PIPE)
for line in pipeData.stdout:
	print(line.decode("utf-8"),end="")

exit(0)
