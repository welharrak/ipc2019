#!/usr/bin/python3
# -*- coding: utf-8 -*-

#walid el harrak

#**03-head-args.py [-n nlin] [-f filein]**

#  Mostar les n primeres línies (o 10)  de filein (o stdin).

#  Definir els dos paràmetres opcionals i les variables on 
#  desar-los. Usar un str default de “/dev/stdin” com a nom 
#  de fitxer per defecte, simplifica el codi, tenim sempre
#  un string.

import sys
import argparse

parser = argparse.ArgumentParser(description='Mostar les n primeres línies (o 10)  de filein (o stdin).',epilog='See you soon baby')

parser.add_argument('-n','--nlines',type=int,dest='nlin',help='Linies a mostrar')

parser.add_argument('-f','--file',type=str,dest='fitxer',help='file to process')

args = parser.parse_args()

###########################

fileIn = sys.stdin



if args.nlin:
	lines = args.nlin
	
if args.fitxer:
	fileIn = open(args.fitxer,'r')

count = 0

for line in fileIn:
	
	count += 1
	
	print(line, end='')
	
	if count == lines:
		break
		
fileIn.close()
