#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# Crear un daytime client/server amb un popen de date. El server plega un cop respost.
# En aquest cas el client pot ser qualsevol eina client que simplement es connecta i
# escolta la resposta del servidor.
# El client es connecta al servidor  i aquest li retorna la data i tanca la conexió. El
# client mostra l adata rebuda i en veure que s'ha tancat la connexió també finalitza.
# El servidor engega i espera a rebre una conexió, quan l'accepta executa un Popen per
# fer un date del sistema operatiu, retorna la informació i tanca la conexió. També
# finalitza (és un servidor molt poc treballador!!)
#
#----------------------------------------------------
import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001 #escoltem pel port 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #stream = TCP
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para que el socket se pueda reutilizar aunque pete


s.bind((HOST,PORT))
s.listen(1) #activa que escolti
conn, addr = s.accept() #comença a escoltar "at infinitum"fins que rebi UN missatge, retorna tupla
print("Connected by", addr)

command = "date"
pipeData = Popen(command,stdout=PIPE,shell=True) #shell=True, lo ejecuta con bash

for line in pipeData.stdout:
	conn.send(line)  
	
conn.close() #tanca la connexió

sys.exit(0)
