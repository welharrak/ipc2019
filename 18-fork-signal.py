#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# El padre tiene el hijo y muere
# Crea un proces fill infinit i se procesara con dos señales:
	# SIGUSR1: printa hola y continua
	# SIGUSR2: printa adios y acaba
#----------------------------------------------------
import sys,os,signal

def myhola(signum,frame):
    print("Signal handler called with signal: %s" % signum)
    print("Que paso pana!")

def mybye(signum,frame):
    print("Signal handler called with signal: %s" % signum)
    print("Hasta luego tron!")
    sys.exit(1)

print("Proces pare: ",os.getpid())
pid = os.fork()

if pid == 0:
    print("Proces fill: ",os.getpid())
    signal.signal(signal.SIGUSR1,myhola)
    signal.signal(signal.SIGUSR2,mybye)
    while True:
        pass

sys.exit(0)
