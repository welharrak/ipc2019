#!/usr/bin/python3
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2019-2020
# Gener 2020
# Echo server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select, os

HOST = ''                 
PORT = 50007             
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
conns=[s] #s es el socket que escolta
while True:
    actius,x,y = select.select(conns,[],[]) #actius conté una llista de les connexions/sockets on hi ha activitat
    for actual in actius: # per cada s que s'ha mogut (s es cada connexion nueva!!!)
        if actual == s: # si es una nova conexio
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else: #en les connexions ja obertes previament
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close() #tanca connexio
                conns.remove(actual) #treure clinet 
            else:
                actual.sendall(data)
                actual.sendall(chr(4),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)


