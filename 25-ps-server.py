#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

#**25-ps-server-one2one.py [-p port]**

# Els clients es connecten a un servidor, envien un informe consistent en fer
# "ps ax" i finalitzen la connexió. El servidor rep l'informe del client i
# el desa a disc. Cada informe es desa amb el format: ip-port-timestamt.log, on
# timestamp té el format AADDMM-HHMMSS.
# Usar un servidor com el de l'exercici anterior, un daemon governat per senyals.
#
#----------------------------------------------------
import sys,socket, argparse,time,signal,os
from subprocess import Popen, PIPE
#-------------------------------------------------------------------------------
def llista_peers(signum,frame):
	print("Signal handler called with signal:", signum)
	print(llistaPeers)

def count_peers(signum,frame):
	print("Signal handler called with signal:", signum)
	print(len(llistaPeers))

def peers(signum,frame):
	print("Signal handler called with signal:", signum)
	print(llistaPeers, len(llistaPeers))
	print("Bye!!")
	sys.exit(0)
	
pid=os.fork()
if pid !=0:
  print("Engegat el server CAL:", pid)
  sys.exit(0)


signal.signal(signal.SIGUSR1,llista_peers)
signal.signal(signal.SIGUSR2,count_peers)
signal.signal(signal.SIGTERM,peers)

#------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description='Server ps')
parser.add_argument('-p','--port',type=int,help='port server',dest='port',default=50001)

args = parser.parse_args()

#----------------------------------------------------------------------------------------
HOST = ''
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #stream = TCP
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para que el socket se pueda reutilizar aunque pete

s.bind((HOST,PORT))
s.listen(1) #activa que escolti

llistaPeers = []
while True:
	conn, addr = s.accept() #comença a escoltar "at infinitum"fins que rebi UN missatge, retorna tupla
	llistaPeers.append(addr)
	print("Connected by", addr)
	
	fileName="/tmp/prova/%s-%s-%s.log" % (addr[0],addr[1],time.strftime("%Y-%m-%d--%H:%M"))
	fileIn = open(fileName,'w')
	while True:
		data = conn.recv(1024)
		if not data: break
		fileIn.write(str(data))
	fileIn.close()
	conn.close()
	
sys.exit(0)
