#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

#Ídem exercici anterior, generar un daytime-server que accepta múltiples clients
#correlatius, és a dir, un un cop finalitzat l'anteior: One2one.
#------------------------------------------------------------------------------------
import sys,socket,os
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001 #escoltem pel port 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #stream = TCP
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para que el socket se pueda reutilizar aunque pete

print(os.getpid())

s.bind((HOST,PORT))

command = "date"
s.listen(1) #activa que escolti

while True:
	conn, addr = s.accept() #comença a escoltar "at infinitum"fins que rebi UN missatge, retorna tupla
	print("Connected by", addr)
	
	pipeData = Popen(command,stdout=PIPE,shell=True) #shell=True, lo ejecuta con bash
	
	for line in pipeData.stdout:
		conn.send(line)  
		
	conn.close()
	

sys.exit(0)

