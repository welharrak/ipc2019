#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak


# SIGUSR1 : +1 minut
# SIGUSR2 : -1 minut
# SIGHUP: retorna alarma a valor inicial
# SIGTERM : no mata, muestra quantos segundos quedan
# SIGALRM: muestra up(timpo sumado) i down i acaba
# No hace caso a ctrl + c
#--------------------------------------------
import sys,os, signal, argparse
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()

global upp
global down
upp = 0
down = 0

def mysigusr1(signum,frame):
  global upp
  print("Signal handler called with signal:", signum)
  upp+=1
  actual=signal.alarm(0)
  signal.alarm(actual+60)

def mysigusr2(signum,frame):
  global down
  print("Signal handler called with signal:", signum)
  down+=1
  actual=signal.alarm(0)
  if actual-60<0:
	  print("no es pot!: %d" % actual)
	  signal.alarm(actual)
  else:
    signal.alarm(actual-60)

def mysighup(signum,frame):
  print("Signal handler called with signal:", signum)
  signal.alarm(args.segons)

def mysigterm(signum,frame):
  print("Signal handler called with signal:", signum)
  falta=signal.alarm(0)
  signal.alarm(falta)
  print("Falten %d segons" % (falta))
 
def mysigalarm(signum,frame):
  global upp, down
  print("Signal handler called with signal:", signum)
  print("Fin:\n upp:%d\n down:%d" % (upp, down))
  sys.exit(1)

signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGHUP,mysighup)
signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGALRM,mysigalarm)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(args.segons)
print(os.getpid())

while True:
  # fem un open que es queda encallat
  pass
signal.alarm(0)
sys.exit(0)

	  
