#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak

# **20-execv-signal.py**
# Usant l'exemple execv programar un procés pare que llança un fill i finalitza.
# El procés fill executa amb execv el programa python 16-signal.py al que li
# passa un valor hardcoded de segons.
#----------------------------------------------------
import sys,os, argparse
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

parser = argparse.ArgumentParser(description="proces fill executa un altre programa",epilog="Oh shet")

parser.add_argument("seconds",type=str,help="seconds")

args = parser.parse_args()

pid = os.fork()

if pid == 0:
    print("Proces fill: ",os.getpid())
    os.execlp("python3","python3","16-signal-time.py",args.seconds)
    sys.exit(0)

sys.exit(0)
