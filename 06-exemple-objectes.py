#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#walid el harrak

#**06-exemple-objectes.py 

#Exemple programació d'Objectes (POO)

class UnixUser(): #Nom comença amb majuscules!!!!
	'''
	Classe UnixUser: prototipo de /etc/passwd --> login:passwd:uid:gid:gecos:home:shell
	'''
	def __init__(self,l,i,g):
		'Constructor objectes Unixuser'
		self.login=l
		self.uid=i
		self.gid=g
		
	def show(self):
		'Mostra dades de user'
		print('login:%s uid:%d gid:%d' % (self.login,self.uid,self.gid))

#---EXEMPLE--
#>>> user1=UnixUser('marta',1000,100)
#>>> user1
#<__main__.UnixUser object at 0x7f29a85f2e10>
#>>> user1.show()
#login:marta uid:1000 gid:100
#>>> user2=user1                       No se crean dos objetos diferents,
#>>> user2                             sino que los dos apuntan a un mismo objeto
#<__main__.UnixUser object at 0x7f29a85f2e10>

	def sumaun(self):
		'Suma +1 al uid'
		
		self.uid += 1
		
	def __str__(self):
		'Mostra per retornar str del objecte'
		
		return "%s %d %d" % (self.login,self.uid,self.gid)
		
