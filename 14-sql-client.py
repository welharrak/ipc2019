#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  walid el harrak


# 14.py -c 2011 -c 2012

# /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
# psql -qtA -F';' training -c "select * from clientes;"
# psql -qtA -F';' -h host -U user training -c "select * from clientes;"
# -------------------------------------------
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument('-c','--client',help='Sentència SQL a executar',dest='llistaClie',action='append',required=True)
#parser.add_argument('-d','--database',help='Sentència SQL a executar',dest='database',required=True)

args = parser.parse_args()

comanda = "psql -qtA -F',' -h 172.17.0.2 -U postgres  training"

pipeData = Popen(comanda,shell=True,stdin=PIPE,stdout=PIPE,bufsize=0,universal_newlines=True)

for client in args.llistaClie:
	comandaClie = "select * from clientes where num_clie=%s;" % client
	pipeData.stdin.write(comandaClie+"\n")
	print(pipeData.stdout.readline(),end='')

pipeData.stdin.write("\q\n")

