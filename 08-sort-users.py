#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# walid el harrak

# 08-sort-users.py [-s login(por defecto) | gid] file
# mostrar lista depende de lo puesto
# ordenacion estable: desempatar por campo clave (irrepetible), para que sea una ordenación 'perfecta'

import sys, argparse

# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
    
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, uid:%d, gid=%d" % (self.login, self.uid, self.gid))

  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
# -------------------------------------------------------

def ord_login(user):
		return user.login
	
def ord_gid(user):
		return (user.gid, user.login)
	
parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",epilog="thats all folks")
		
parser.add_argument('-s','--sort',type=str,dest='sort',help='Mostrar dependen del ordre posat',choices=['login','gid'],default='login')

parser.add_argument("file",type=str,help="user file(/etc/passwd style)", metavar="file")

args=parser.parse_args()

fileIn=open(args.file,"r")
userList=[]
for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
fileIn.close()

if args.sort == 'gid':
	userList.sort(key=ord_gid)
else:
	userList.sort(key=ord_login)

for user in userList:
	print(user)


