#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  walid el harrak


# exemple signal: associacio de signals a funcions
#--------------------------------------------
import sys,os,signal

def myhandler(signum,frame):
	print("Signal handler called with signal: %s" % signum)
	print("See you soon Baby")
	sys.exit(1)

def mydeath(signum,frame):
	print("Signal handler called with signal: %s" % signum)
	print("I'm not going to die b*tch")

#assignar senyal a funcio
signal.signal(signal.SIGALRM,myhandler) #14 ejecuta myhandler
signal.signal(signal.SIGUSR2,myhandler)  #12   "    "     " 
signal.signal(signal.SIGUSR1,mydeath)    #10    "     mydeath 
signal.signal(signal.SIGINT,mydeath)
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,signal.SIG_IGN) #SIG_IGN: ignora senyal
signal.alarm(60) #cuando pasan 60 segundos, envia señal SIGALRM y se acaba el proceso

print(os.getpid())

while True:
	pass
	
signal.alarm(0)
sys.exit(0)
