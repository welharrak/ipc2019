#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  walid el harrak


# 13.py 'select * from ...'

# /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
# psql -qtA -F';' training -c "select * from clientes;"
# psql -qtA -F';' -h host -U user training -c "select * from clientes;"

import argparse
from subprocess import Popen, PIPE
# -------------------------------------------------------

parser = argparse.ArgumentParser(description="programa que fa consultes a training",epilog="postgresql")

parser.add_argument('comandaSql',type=str,metavar='comandasql',help='groupfile')

args = parser.parse_args()

#command = "psql -qtA -F',' -h 172.17.0.2 -U postgres training -c '%s' " % args.comandaSql
command = "psql -qtA -F',' -h 172.17.0.2 -U postgres training "

pipeData = Popen(command,stdout=PIPE,bufsize=0,universal_newlines=True,stdin=PIPE,shell=True) #shell=True, lo ejecuta con bash

pipeData.stdin.write(args.comandaSql+'\n\q\n')


for line in pipeData.stdout:
  print(line,end='')

