#!/usr/bin/python3
# -*- coding: utf-8 -*-

#walid el harrak

import sys

LINIES = 10

#PROGRAMA PRINCIPAL

#ponemos que fitxer es stdin
fitxer = sys.stdin

#si tenemos un fitxero, que este sea fitxer i hacer open
if len(sys.argv) == 2:
	
	fitxer = open(sys.argv[1],'r')
	
count = 0

#por cada linea de fitxero, printar linia
for linia in fitxer:
	
	count += 1
	
	print(linia, end="")
	
	#si hemos llegado a las linias especificas, que salga	
	if count == LINIES: 
		break

fitxer.close()

